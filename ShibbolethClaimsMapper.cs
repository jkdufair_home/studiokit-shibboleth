﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace StudioKit.Shibboleth
{
	/// <inheritdoc />
	/// <summary>
	/// Maps Shibboleth Attributes to common <see cref="ClaimTypes" /> at SP authentication time.
	/// </summary>
	public class ShibbolethClaimsMapper : ClaimsAuthenticationManager
	{
		private static readonly Dictionary<string, string> ShibbolethAttributeClaimMap = new Dictionary<string, string>
		{
			{ShibbolethSaml2Attributes.Uid, ClaimTypes.Name},
			{ShibbolethSaml2Attributes.Mail, ClaimTypes.Email},
			//{ShibbolethSaml2Attributes.DisplayName, ""},
			{ShibbolethSaml2Attributes.GivenName, ClaimTypes.GivenName},
			{ShibbolethSaml2Attributes.Sn, ClaimTypes.Surname},
			//{ShibbolethSaml2Attributes.Cn, ""},
			{ShibbolethSaml2Attributes.EmployeeNumber, ClaimTypes.NameIdentifier},
			//{ShibbolethSaml2Attributes.EmployeeType, ""},
			{ShibbolethSaml2Attributes.EduPersonPrincipalName, ClaimTypes.Name},
			//{ShibbolethSaml2Attributes.EduPersonScopedAffiliation, ""},
			//{ShibbolethSaml2Attributes.EduPersonTargetedId, ""},
			//{ShibbolethSaml2Attributes.EduCourseOffering, ""}
		};

		private static IEnumerable<Claim> GetCommonClaims(IEnumerable<Claim> claims)
		{
			return claims
				.Where(c => !string.IsNullOrWhiteSpace(c.Value))
				.Join(ShibbolethAttributeClaimMap, c => c.Type, kvp => kvp.Key, (c, kvp) => new Claim(kvp.Value, c.Value, c.ValueType, c.Issuer, c.OriginalIssuer, c.Subject))
				.ToList();
		}

		public override ClaimsPrincipal Authenticate(string resourceName, ClaimsPrincipal incomingPrincipal)
		{
			foreach (var identity in incomingPrincipal.Identities)
				identity.AddClaims(GetCommonClaims(identity.Claims));

			return base.Authenticate(resourceName, incomingPrincipal);
		}
	}
}