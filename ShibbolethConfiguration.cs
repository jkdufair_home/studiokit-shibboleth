﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Metadata;
using System.Security.Cryptography.X509Certificates;
using Kentor.AuthServices;
using Kentor.AuthServices.Configuration;
using Kentor.AuthServices.Metadata;
using Kentor.AuthServices.Owin;
using Kentor.AuthServices.Saml2P;
using Owin;
using StudioKit.Encryption;

namespace StudioKit.Shibboleth
{
	public static class ShibbolethConfiguration
	{
		/// <summary>
		/// Is shibboleth enabled, based on existence of app settings
		/// </summary>
		public static bool IsEnabled => !string.IsNullOrWhiteSpace(EntityId);

		/// <summary>
		/// The SP entityID
		/// </summary>
		private static string EntityId => EncryptedConfigurationManager.GetSetting("ShibbolethSpEntityId");

		/// <summary>
		/// The default return url to the SP during authentication
		/// </summary>
		private static string PublicOrigin => EncryptedConfigurationManager.GetSetting("ShibbolethSpPublicOrigin");

		/// <summary>
		/// A comma-separated list of federation urls
		/// </summary>
		private static string FederationUrls => EncryptedConfigurationManager.GetSetting("ShibbolethSpFederationUrls");

		/// <summary>
		/// The thumbprint for the SP signing certificate
		/// </summary>
		private static string CertificateThumbrint => EncryptedConfigurationManager.GetSetting("ShibbolethSpCertificateThumbprint");

		/// <summary>
		/// For use with Azure Web Apps, a comma-separated list of certificate thumbprints loaded for the web app. Must include <see cref="CertificateThumbrint"/>.
		/// </summary>
		private static string LoadedCertificateThumbprints => EncryptedConfigurationManager.GetSetting("WEBSITE_LOAD_CERTIFICATES");

		public static void ConfigureShibboleth(IAppBuilder app)
		{
			app.UseKentorAuthServicesAuthentication(GetAuthServicesOptions());
		}

		private static KentorAuthServicesAuthenticationOptions GetAuthServicesOptions()
		{
			var authServicesOptions = new KentorAuthServicesAuthenticationOptions(false)
			{
				SPOptions = GetSpOptions()
			};

			//
			// IDPs
			//

			// Add Idps not included in Federations here

			//
			// Federation(s)
			//

			foreach (var federationUrl in FederationUrls.Split(','))
			{
				// It's enough to just create the federation and associate it
				// with the options. The federation will load the metadata and
				// update the options with any identity providers found.
				// ReSharper disable once UnusedVariable
				var federation = new Federation(federationUrl, true, authServicesOptions);
			}

			return authServicesOptions;
		}

		private static SPOptions GetSpOptions()
		{
			var spOptions = new SPOptions
			{
				EntityId = new EntityId(EntityId),
				ModulePath = "/shibboleth",
				PublicOrigin = new Uri(PublicOrigin),
				NameIdPolicy = new Saml2NameIdPolicy(null, NameIdFormat.Transient),
				Organization = GetOrganization()
			};

			// Map Shibboleth attributes to claims using a custom claims manager: https://github.com/KentorIT/authservices/blob/master/doc/ClaimsAuthenticationManager.md
			spOptions.SystemIdentityModelIdentityConfiguration.ClaimsAuthenticationManager =
				new ShibbolethClaimsMapper();

			foreach (var contact in GetContacts())
				spOptions.Contacts.Add(contact);

			spOptions.AttributeConsumingServices.Add(GetAttributeConsumingService());

			spOptions.ServiceCertificates.Add(LoadCertificate());

			return spOptions;
		}

		private static Organization GetOrganization()
		{
			var en = CultureInfo.GetCultureInfo("en-US");
			var organization = new Organization();
			organization.Names.Add(new LocalizedName("Purdue University Main Campus", en));
			organization.DisplayNames.Add(new LocalizedName("Purdue University Main Campus", en));
			organization.Urls.Add(new LocalizedUri(new Uri("http://www.purdue.edu"), en));
			return organization;
		}

		private static IEnumerable<ContactPerson> GetContacts()
		{
			var techContact = new ContactPerson
			{
				Type = ContactType.Technical,
				GivenName = "Purdue University Identity and Access Management"
			};
			techContact.EmailAddresses.Add("accounts@purdue.edu");

			var otherContact = new ContactPerson
			{
				Type = ContactType.Other,
				GivenName = "Purdue Security Team"
			};
			otherContact.EmailAddresses.Add("abuse@purdue.edu");

			return new List<ContactPerson> { techContact, otherContact };
		}

		private static AttributeConsumingService GetAttributeConsumingService()
		{
			var attributeConsumingService = new AttributeConsumingService("AuthServices")
			{
				IsDefault = true
			};
			attributeConsumingService.RequestedAttributes.Add(
				new RequestedAttribute(ShibbolethSaml2Attributes.Uid)
				{
					FriendlyName = "uid",
					NameFormat = RequestedAttribute.AttributeNameFormatUri
				});
			attributeConsumingService.RequestedAttributes.Add(
				new RequestedAttribute(ShibbolethSaml2Attributes.Mail)
				{
					FriendlyName = "mail",
					NameFormat = RequestedAttribute.AttributeNameFormatUri,
					IsRequired = true
				});
			attributeConsumingService.RequestedAttributes.Add(
				new RequestedAttribute(ShibbolethSaml2Attributes.DisplayName)
				{
					FriendlyName = "displayName",
					NameFormat = RequestedAttribute.AttributeNameFormatUri
				});
			attributeConsumingService.RequestedAttributes.Add(
				new RequestedAttribute(ShibbolethSaml2Attributes.Cn)
				{
					FriendlyName = "cn",
					NameFormat = RequestedAttribute.AttributeNameFormatUri
				});
			attributeConsumingService.RequestedAttributes.Add(
				new RequestedAttribute(ShibbolethSaml2Attributes.Sn)
				{
					FriendlyName = "sn",
					NameFormat = RequestedAttribute.AttributeNameFormatUri
				});
			attributeConsumingService.RequestedAttributes.Add(
				new RequestedAttribute(ShibbolethSaml2Attributes.GivenName)
				{
					FriendlyName = "givenName",
					NameFormat = RequestedAttribute.AttributeNameFormatUri
				});
			attributeConsumingService.RequestedAttributes.Add(
				new RequestedAttribute(ShibbolethSaml2Attributes.EmployeeNumber)
				{
					FriendlyName = "employeeNumber",
					NameFormat = RequestedAttribute.AttributeNameFormatUri
				});
			attributeConsumingService.RequestedAttributes.Add(
				new RequestedAttribute(ShibbolethSaml2Attributes.EmployeeType)
				{
					FriendlyName = "employeeType",
					NameFormat = RequestedAttribute.AttributeNameFormatUri
				});
			attributeConsumingService.RequestedAttributes.Add(
				new RequestedAttribute(ShibbolethSaml2Attributes.EduPersonPrincipalName)
				{
					FriendlyName = "eduPersonPrincipalName",
					NameFormat = RequestedAttribute.AttributeNameFormatUri,
					//AttributeValueXsiType = "ScopedAttributeDecoder"
				});
			attributeConsumingService.RequestedAttributes.Add(
				new RequestedAttribute(ShibbolethSaml2Attributes.EduPersonScopedAffiliation)
				{
					FriendlyName = "eduPersonScopedAffiliation",
					NameFormat = RequestedAttribute.AttributeNameFormatUri
				});
			attributeConsumingService.RequestedAttributes.Add(
				new RequestedAttribute(ShibbolethSaml2Attributes.EduPersonTargetedId)
				{
					FriendlyName = "eduPersonTargetedID",
					NameFormat = RequestedAttribute.AttributeNameFormatUri
				});
			attributeConsumingService.RequestedAttributes.Add(
				new RequestedAttribute(ShibbolethSaml2Attributes.EduCourseOffering)
				{
					FriendlyName = "eduCourseOffering",
					NameFormat = RequestedAttribute.AttributeNameFormatUri
				});
			return attributeConsumingService;
		}

		private static X509Certificate2 LoadCertificate()
		{
			if (!string.IsNullOrWhiteSpace(LoadedCertificateThumbprints) && !LoadedCertificateThumbprints.Contains(CertificateThumbrint))
				throw new Exception("WEBSITE_LOAD_CERTIFICATES does not contain ShibbolethSpCertificateThumbprint");
			return string.IsNullOrWhiteSpace(CertificateThumbrint)
				? LoadCertificateFromFile()
				: LoadCertificateFromCloud(CertificateThumbrint);
		}

		private static X509Certificate2 LoadCertificateFromFile()
		{
			// TODO: remove test certificate
			return new X509Certificate2($"{AppDomain.CurrentDomain.SetupInformation.ApplicationBase}/App_Data/Kentor.AuthServices.Tests.pfx");
		}

		/// <summary>
		/// Loads certificate from the azure certificate store.
		/// See https://azure.microsoft.com/en-us/blog/using-certificates-in-azure-websites-applications/
		/// Note: Need to have config setting WEBSITE_LOAD_CERTIFICATES with a value set to the certificate thumbprint
		/// </summary>
		/// <returns></returns>
		private static X509Certificate2 LoadCertificateFromCloud(string thumbprint)
		{
			X509Certificate2 certificate = null;

			var certStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
			certStore.Open(OpenFlags.ReadOnly);
			var certCollection = certStore.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false);
			// Get the first cert with the thumbprint
			if (certCollection.Count > 0)
			{
				certificate = certCollection[0];
			}
			certStore.Close();
			return certificate;
		}
	}
}