﻿namespace StudioKit.Shibboleth
{
	public static class ShibbolethSaml2Attributes
	{
		public const string Uid = "urn:oid:0.9.2342.19200300.100.1.1";

		public const string Mail = "urn:oid:0.9.2342.19200300.100.1.3";

		public const string DisplayName = "urn:oid:2.16.840.1.113730.3.1.241";

		public const string Cn = "urn:oid:2.5.4.3";

		public const string Sn = "urn:oid:2.5.4.4";

		public const string GivenName = "urn:oid:2.5.4.42";

		public const string EmployeeNumber = "urn:oid:2.16.840.1.113730.3.1.3";

		public const string EmployeeType = "urn:oid:2.16.840.1.113730.3.1.4";

		public const string EduPersonPrincipalName = "urn:oid:1.3.6.1.4.1.5923.1.1.1.6";

		public const string EduPersonScopedAffiliation = "urn:oid:1.3.6.1.4.1.5923.1.1.1.9";

		public const string EduPersonTargetedId = "urn:oid:1.3.6.1.4.1.5923.1.1.1.10";

		public const string EduCourseOffering = "urn:oid:1.3.6.1.4.1.5923.1.6.1.1";
	}
}